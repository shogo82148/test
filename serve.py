#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cgi
import json
import subprocess

import smtplib
from email.MIMEText import MIMEText
from email.Utils import formatdate

def create_message(from_addr, to_addr, subject, body):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Date'] = formatdate()
    return msg

def send(from_addr, to_addr, msg):
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('shogo82148.kslab@gmail.com', 'rI0qVppF')
    s.sendmail(from_addr, [to_addr], msg.as_string())
    s.close()

class MyApplication(object):
    def __call__(self, env, response):
        method = env['REQUEST_METHOD']

        if method == 'GET':
            return self.do_get(env, response)
        elif method == 'POST':
            return self.do_post(env, response)
        else:
            response('501 Not Implemented', [('Content-type', 'text/plain')])
            return 'Not Implemented'

    def do_get(self, env, response):
        self.process_payload({})
        response('200 OK', [('Content-type', 'text/plain')])
        return 'ok'

    def do_post(self, env, response):
        data = env['wsgi.input']
        length = int(env.get('CONTENT_LENGTH', 0))
        query = dict(cgi.parse_qsl(data.read(length)))
        payload = json.loads(query['payload'])

        self.process_payload(payload)

        response('200 OK', [('Content-type', 'text/plain')])
        return 'ok'

    def process_payload(self, payload):
        #if payload['canon_url'] != 'https://bitbucket.org':
        #    raise Exception()
        #if payload['repository']['absolute_url'] != '/shogo82148/homepage/':
        #    raise Exception()

        result = ''
        result += self.execute('git pull origin master')
        result += self.execute('./template.sh')
        #result += self.execute('./uploader/uploader.pl')
        print result
        #return

        from_addr = 'shogo82148.kslab@gmail.com'
        to_addr = 'shogo82148@gmail.com'
        msg = create_message(
            from_addr,
            to_addr,
            'Webpage generated',
            result)
        send(from_addr, to_addr, msg)

    def execute(self, command):
        proc = subprocess.Popen(
            command,
            shell = True,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE)
        stdout, stderr = proc.communicate()
        return '$ ' + command + '\n*stdout:\n' + stdout + '\n\n*stderr:\n' + stderr + '\n\n'


from wsgiref import simple_server

if __name__ == '__main__':
    simple_server.make_server('', 2222, MyApplication()).serve_forever()
